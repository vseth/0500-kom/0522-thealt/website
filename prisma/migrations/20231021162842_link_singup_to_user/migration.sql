/*
  Warnings:

  - You are about to drop the column `email` on the `SignUp` table. All the data in the column will be lost.
  - You are about to drop the column `firstName` on the `SignUp` table. All the data in the column will be lost.
  - You are about to drop the column `lastName` on the `SignUp` table. All the data in the column will be lost.
  - You are about to drop the column `sub` on the `SignUp` table. All the data in the column will be lost.
  - Added the required column `userId` to the `SignUp` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE `SignUp` DROP COLUMN `email`,
    DROP COLUMN `firstName`,
    DROP COLUMN `lastName`,
    DROP COLUMN `sub`,
    ADD COLUMN `userId` VARCHAR(191) NOT NULL;

-- AddForeignKey
ALTER TABLE `SignUp` ADD CONSTRAINT `SignUp_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `User`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
