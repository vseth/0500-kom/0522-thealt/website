#!/bin/bash

echo "DATABASE_URL=mysql://$SIP_MYSQL_ALT_USER:$SIP_MYSQL_ALT_PW@$SIP_MYSQL_ALT_SERVER:$SIP_MYSQL_ALT_PORT/$SIP_MYSQL_ALT_NAME?schema=public" > .env
echo "NEXT_PUBLIC_API_URL=$NEXT_PUBLIC_API_URL" >> .env
echo "NEXT_PUBLIC_DEPLOYMENT=$NEXT_PUBLIC_DEPLOYMENT" >> .env

npx prisma migrate deploy
npx prisma generate
npm run start
