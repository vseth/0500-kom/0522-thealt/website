"use client";

import { Container } from "@mantine/core";

import Header from "/components/header";
import Events from "/components/events";
import About from "/components/about";
import Philosophy from "/components/philosophy";

export default function Page({ params: { locale } }) {
  return (
    <div>
      <section className="light" style={{ paddingTop: 0 }}>
        <Container size="xl">
          <Header />
        </Container>
      </section>
      <section className="dark" id="events">
        <Container size="xl">
          <Events />
        </Container>
      </section>
      <section className="light" id="about">
        <Container size="xl">
          <About />
        </Container>
      </section>
      <section className="dark" id="philosophy">
        <Container size="xl">
          <Philosophy />
        </Container>
      </section>
    </div>
  );
}
