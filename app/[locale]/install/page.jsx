"use client";

import "/app/guides.css";

import dynamic from "next/dynamic";
import { useState, useEffect } from "react";

import { Alert, Container, Grid, MediaQuery } from "@mantine/core";

import { IconInfoCircle } from "@tabler/icons-react";

import InstallGuide from "/components/installguide";
const TOC = dynamic(() => import("/components/toc"), {
  ssr: false,
});

export default function Install({ params: { locale } }) {
  return (
    <>
      <Container size="xl" className="my-8">
        <div className="grid gap-4 grid-cols-1 md:grid-cols-3">
          <div className="md:block hidden">
            <TOC />
          </div>
          <div className="bash-guide col-span-1 md:col-span-2">
            {locale === "de" && (
              <Alert icon={<IconInfoCircle />} title="Bemerkung">
                Leider ist dieser Guide nur auf Englisch verfügbar.
              </Alert>
            )}
            <InstallGuide />
          </div>
        </div>
      </Container>
    </>
  );
}
