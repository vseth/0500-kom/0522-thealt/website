import dayjs from "dayjs";

const utc = require("dayjs/plugin/utc");
const customParseFormat = require("dayjs/plugin/customParseFormat");
const timezone = require("dayjs/plugin/timezone");
dayjs.extend(utc);
dayjs.extend(customParseFormat);
dayjs.extend(timezone);

require("dayjs/locale/en");
require("dayjs/locale/de");

export function formatDateFromDB(date, locale) {
  dayjs.locale(locale);
  const d = dayjs(date).tz("Europe/Zurich");
  return locale == "de" ? d.format("dd, DD.MM.YY") : d.format("ddd, DD.MM.YY");
}

export function getTimeString(time) {
  const t = dayjs(time).tz("Europe/Zurich");
  return t.format("HH:mm");
}

export function formatTimeFromDB(startTime, endTime) {
  return getTimeString(startTime) + " – " + getTimeString(endTime);
}

export function isInFuture(event) {
  const now = new Date();
  now.setDate(now.getDate() - 1);
  return event.date > now.toISOString();
}
