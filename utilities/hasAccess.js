export default function hasAccess(session, admin) {
  const stag_role = "thealt_staging_website_website";
  const prod_role = "thealt_prod_website_website";

  // this is just for development purposes
  // return session ? true : false;

  if (process.env.NODE_ENV && process.env.NODE_ENV === "development")
    return session ? true : false;

  if (!session) return false; // not logged in --> no access
  if (!admin) {
    // if no admin is required and is logged in --> access
    return true;
  } else {
    if (!session.info) return false;
    if (!session.info.payload) return false;
    if (!session.info.payload.resource_access) return false; // admin required, but no role --> no access
    if (session.info.payload.resource_access[prod_role]) {
      if (session.info.payload.resource_access[prod_role]?.roles[0] != "admin")
        return false; // role incorrect
    } else if (session.info.payload.resource_access[stag_role]) {
      if (session.info.payload.resource_access[stag_role]?.roles[0] != "admin")
        return false; // role incorrect
    }
    return true;
  }
}
