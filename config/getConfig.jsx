const getConfig = async () => {
  const res = await fetch(
    "https://static.vseth.ethz.ch/assets/vseth-0522-thealt/config.json",
    {
      next: {
        revalidate: 86400,
      },
    },
  );

  return res.json();
};

export default getConfig;
