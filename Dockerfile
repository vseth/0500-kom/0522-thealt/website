FROM node:20

RUN mkdir -p /website
WORKDIR /website
COPY . /website

RUN npm install
RUN npm run build

EXPOSE 3000

CMD ["bash", "entrypoint.sh"]
