import { useRouter } from "next/router";

import { Blockquote, Space } from "@mantine/core";

import parse from "html-react-parser";

import { useCurrentLocale } from "/locales/client";

import philosophy from "/content/philosophy";

export default function Philosophy() {
  const locale = useCurrentLocale();

  return (
    <>
      {philosophy.map((entry, i) => (
        <div key={i}>
          <h2 className="font-bold text-4xl">{entry.title[locale || "en"]}</h2>

          <div className="my-4">
            <Blockquote cite={"— " + entry.source} color="orange">
              {entry.summary[locale || "en"]}
            </Blockquote>
          </div>

          {entry.definition[locale || "en"].map((def, i) => (
            <p key={i}>{parse(def)}</p>
          ))}

          <Space h="md" />
          <p>{parse(entry.examples[locale || "en"])}</p>

          <Space h="xl" />
          <Space h="xl" />
        </div>
      ))}
    </>
  );
}
