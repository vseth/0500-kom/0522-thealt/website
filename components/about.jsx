import dynamic from "next/dynamic";

const OfficeMap = dynamic(() => import("/components/map"), {
  ssr: false,
});

import { useI18n, useCurrentLocale } from "/locales/client";

import parse from "html-react-parser";

import { Alert, Button, Card, Paper, Space, Text } from "@mantine/core";

import ContactForm from "/components/contactForm";
import BoardMemberCard from "/components/boardMemberCard";

import board from "/content/board.json";
import about from "/content/about.json";

export default function About() {
  const t = useI18n();
  const locale = useCurrentLocale();

  return (
    <>
      <h1 className="font-black text-6xl">{t("about")}</h1>

      <div className="flex flex-wrap -m-2 mb-6 justify-center">
        {board.map((entry, i) => (
          <div
            key={i}
            className="flex flex-col basis-full sm:basis-6/12 xl:basis-3/12 grow-0 shrink-0 p-2"
          >
            <BoardMemberCard entry={entry} />
          </div>
        ))}
      </div>

      <Space h="xl" />
      <Space h="xl" />
      <section id="address">
        <div className="grid gap-4 grid-cols-1 md:grid-cols-3">
          <div>
            <Paper shadow="md">
              <Alert title={t("aboutUs")} variant="outline">
                <Text>{about.description[locale || "en"]}</Text>
              </Alert>
            </Paper>

            <Space h="md" />
            <b>TheAlternative</b>
            {about.address.map((line, i) => (
              <p key={i} className="my-0">
                {line}
              </p>
            ))}

            <Space h="xs" />
            {/* stolen from here: https://stackoverflow.com/questions/33577448/is-there-a-way-to-do-array-join-in-react */}
            {about.links
              .map((link, i) => (
                <a target="_blank" href={link.url} key={i}>
                  {link.text}
                </a>
              ))
              .reduce(
                (acc, x) =>
                  acc === null ? (
                    x
                  ) : (
                    <>
                      {acc} | {x}
                    </>
                  ),
                null,
              )}
          </div>
          <div>
            <OfficeMap />
          </div>
          <div>
            <ContactForm />
          </div>
        </div>
      </section>
    </>
  );
}
