import Link from "next/link";

import NavDesktop from "/components/navDesktop";
import NavMobile from "/components/navMobile";
import I18nWrapper from "/components/i18nWrapper";
import LoginButton from "/components/loginButton";

import { getI18n } from "/locales/server";

import getConfig from "/config/getConfig";

const Logo = () => (
  <>
    <img
      alt="TheAlternative Logo"
      src="/images/logo.svg"
      className="h-8 block dark:hidden"
    />
    <img
      alt="TheAlternative Logo"
      src="/images/logo-inv.svg"
      className="h-8 hidden dark:block"
    />
  </>
);

export default async function Navbar({ locale }) {
  const t = await getI18n();

  const config = await getConfig();

  const navItems = [
    { label: t("bashGuide"), href: "/bash" },
    { label: t("installGuide"), href: "/install" },
    { label: t("linuxAtETH"), href: "https://gitlab.ethz.ch/thealternative/linux-at-eth" },
    {
      label: t("courses"),
      href: "https://gitlab.ethz.ch/thealternative/courses",
    },
  ];

  return (
    <>
      <div
        style={{ backgroundColor: config.primaryColor }}
        className="hidden md:block w-full h-14 px-4"
      >
        <div className="flex justify-between items-center h-full">
          <img src={"/images/vseth_inv_nobyline.svg"} className="h-8" />

          <NavDesktop items={config.externalNav} locale={locale} />
        </div>
      </div>
      <div className="w-full bg-white dark:bg-zinc-900 shadow-md h-14 px-4 sticky top-0 bg-opacity-90 dark:bg-opacity-90 backdrop-blur-lg backdrop-filter hidden md:block z-40 dark:border-b border-b-zinc-400">
        <div className="flex justify-between items-center h-full">
          <Link className="text-xl text-zinc-700 dark:text-zinc-300" href="/">
            <Logo />
          </Link>
          <div className="md:flex hidden gap-8 text-zinc-700 dark:text-zinc-300 items-center">
            {navItems.map((item, i) => (
              <Link href={item.href} key={i}>
                {item.label}
              </Link>
            ))}

            <I18nWrapper locale={locale} theme={undefined}>
              <LoginButton primary={config.primaryColor} />
            </I18nWrapper>
          </div>
        </div>
      </div>
      <I18nWrapper locale={locale} theme={undefined}>
        <NavMobile
          items={navItems}
          locale={locale}
          signet="/signet-mono-inv.svg"
          primary={config.primaryColor}
          title={
            <img
              alt="TheAlternative Logo"
              src="/images/logo-inv.svg"
              className="h-8"
            />
          }
        />
      </I18nWrapper>
    </>
  );
}
