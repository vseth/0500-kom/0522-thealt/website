import { useState } from "react";

import { useSession } from "next-auth/react";

import { useI18n, useCurrentLocale } from "/locales/client";

import { Alert, Button, Space, useMantineTheme } from "@mantine/core";

import { IconPlus, IconInfoCircle } from "@tabler/icons-react";

import EventModal from "/components/eventModal";
import SignUpsModal from "/components/signUpsModal";
import EventCard from "/components/eventCard";
import { getAccentColor } from "/utilities/colors";
import hasAccess from "/utilities/hasAccess";

import { gql, useQuery } from "@apollo/client";

const getFutureEventsQuery = gql`
  query getFutureEvents {
    getFutureEvents {
      id
      title
      speaker
      description
      date
      startTime
      endTime
      place
      signUp
      isStammtisch
      signUps {
        user {
          id
          name
          email
        }
      }
    }
  }
`;

const getUserQuery = gql`
  query getUser {
    getUser {
      id
    }
  }
`;

export default function Events() {
  const { data: events, refetch } = useQuery(getFutureEventsQuery);
  const { data: user } = useQuery(getUserQuery);

  const [open, setOpen] = useState(false);
  const [signUpOpen, setSignUpOpen] = useState(false);
  const [event, setEvent] = useState(null);

  const { data: session } = useSession();

  const theme = useMantineTheme();
  const t = useI18n();
  const locale = useCurrentLocale();

  const addEvent = () => {
    setEvent(null);
    setOpen(true);
  };

  return (
    <>
      <h1 className="font-black text-6xl">{t("events")}</h1>

      {hasAccess(session, true) && (
        <div className="flex justify-center">
          <Button
            leftSection={<IconPlus />}
            onClick={addEvent}
            variant="default"
            className="mt-4"
          >
            {t("addEvent")}
          </Button>
        </div>
      )}

      {locale === "de" && (
        <Alert
          icon={<IconInfoCircle />}
          title="Bemerkung"
          mt="xl"
          variant="filled"
        >
          Bitte beachte, dass unsere Events auf Englisch gehalten werden.
        </Alert>
      )}

      <EventModal
        open={open}
        event={event}
        close={() => setOpen(false)}
        refetch={refetch}
      />
      <SignUpsModal
        open={signUpOpen}
        event={event}
        close={() => setSignUpOpen(false)}
      />

      <Space h="xl" />
      <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-4">
        {events &&
          events.getFutureEvents.map((e) => (
            <div key={e.id} className="flex">
              <EventCard
                event={e}
                setEvent={setEvent}
                setOpen={setOpen}
                refetch={refetch}
                setSignUpOpen={setSignUpOpen}
                user={user?.getUser}
              />
            </div>
          ))}
      </div>
    </>
  );
}
