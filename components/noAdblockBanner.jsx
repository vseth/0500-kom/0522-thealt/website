import { useState, useEffect } from "react";

import { Alert } from "@mantine/core";

import { useI18n } from "/locales/client";

import parse from "html-react-parser";

import { isAdblocking } from "adblock-hunter";

export default function NoAdblockBanner() {
  const t = useI18n();

  const [hasAdblock, setHasAdblock] = useState(true);

  useEffect(() => {
    isAdblocking().then((isAdblocking) => {
      setHasAdblock(isAdblocking);
    });
  }, []);

  return (
    <>
      {!hasAdblock && (
        <Alert title={t("noAdblockTitle")} color="red">
          {parse(t("noAdblockText"))}
        </Alert>
      )}
    </>
  );
}
