import { useEffect } from "react";

import { useI18n } from "/locales/client";

import {
  Button,
  Checkbox,
  Divider,
  Grid,
  Modal,
  Select,
  Space,
  Text,
  Textarea,
  TextInput,
} from "@mantine/core";

import { useForm } from "@mantine/form";
import { DateInput, TimeInput } from "@mantine/dates";

import { useEditor } from "@tiptap/react";
import { Link } from "@mantine/tiptap";
import StarterKit from "@tiptap/starter-kit";
import Underline from "@tiptap/extension-underline";

import Editor from "/components/editor";

import { getTimeString } from "/utilities/dates";

import templates from "/content/eventTemplates.json";

import { gql, useMutation } from "@apollo/client";

const addEventMutation = gql`
  mutation addEvent(
    $title: String
    $speaker: String
    $description: String
    $date: DateTime
    $time: [DateTime]
    $place: String
    $signUp: String
    $isStammtisch: Boolean
  ) {
    addEvent(
      title: $title
      speaker: $speaker
      description: $description
      date: $date
      time: $time
      place: $place
      signUp: $signUp
      isStammtisch: $isStammtisch
    )
  }
`;

const editEventMutation = gql`
  mutation editEvent(
    $id: Int
    $title: String
    $speaker: String
    $description: String
    $date: DateTime
    $time: [DateTime]
    $place: String
    $signUp: String
    $isStammtisch: Boolean
  ) {
    editEvent(
      id: $id
      title: $title
      speaker: $speaker
      description: $description
      date: $date
      time: $time
      place: $place
      signUp: $signUp
      isStammtisch: $isStammtisch
    )
  }
`;

export default function EventModal({ open, close, event, refetch }) {
  const [addEvent] = useMutation(addEventMutation);
  const [editEvent] = useMutation(editEventMutation);

  const t = useI18n();

  const initialValues = {
    title: "",
    speaker: "",
    date: null,
    startTime: null,
    endTime: null,
    place: "",
    signUp: "",
    isStammtisch: false,
  };

  const form = useForm({
    initialValues: initialValues,

    validate: {
      title: (value) => (value ? null : t("ENotEmpty")),
      date: (value) => (value ? null : t("ENotEmpty")),
      startTime: (value) => (!value ? t("ENotEmpty") : null),
      endTime: (value) => (!value ? t("ENotEmpty") : null),
      place: (value) => (value ? null : t("ENotEmpty")),
    },
  });

  const content = "";

  const editor = useEditor({
    extensions: [StarterKit, Underline, Link],
    content,
  });

  const templatesFmt = templates.map((template) => ({
    label: template.title,
    value: template.title,
  }));

  const setTemplate = (t) => {
    const template = templates.filter((temp) => temp.title === t)[0];

    form.setValues({
      ...template,
      startTime: template.time ? template.time[0] : null,
      endTime: template.time ? template.time[1] : null,
    });
    editor.commands.setContent(template.description);
  };

  const setEvent = () => {
    if (event) {
      form.setValues({
        ...event,
        date: new Date(event.date),
        startTime: getTimeString(new Date(event.startTime)),
        endTime: getTimeString(new Date(event.endTime)),
      });
      if (editor) editor.commands.setContent(event.description);
    } else {
      form.setValues(initialValues);
      if (editor) editor.commands.setContent(content);
    }
  };

  useEffect(() => {
    setEvent();
  }, [open]);

  const submit = async (values) => {
    if (event) {
      const res = await editEvent({
        variables: {
          id: event.id,
          ...values,
          time: [values.startTime, values.endTime].map((v) => {
            const d = new Date();
            const split = v.split(":");
            d.setHours(Number(split[0]), Number(split[1]), 0);
            return d;
          }),
          description: editor.getHTML(),
        },
      });
      if (res.data.editEvent) {
        refetch();
        close();
      }
    } else {
      const res = await addEvent({
        variables: {
          ...values,
          time: [values.startTime, values.endTime].map((v) => {
            const d = new Date();
            const split = v.split(":");
            d.setHours(Number(split[0]), Number(split[1]), 0);
            return d;
          }),
          description: editor.getHTML(),
        },
      });
      if (res.data.addEvent) {
        refetch();
        close();
      }
    }
  };

  return (
    <Modal
      opened={open}
      onClose={close}
      size="xl"
      title={event ? t("editEvent") : t("addEvent")}
    >
      <Select
        data={templatesFmt}
        placeholder={t("loadTemplate")}
        onChange={(e) => setTemplate(e)}
      />

      <Space h="xl" />

      <Divider />

      <Space h="xs" />

      <form onSubmit={form.onSubmit((values) => submit(values))}>
        <div className="grid gap-4 grid-cols-1 md:grid-cols-2">
          <div>
            <TextInput
              withAsterisk
              label={t("title")}
              placeholder="Introduction to Free Software"
              {...form.getInputProps("title")}
            />
          </div>
          <div>
            <TextInput
              withAsterisk
              label={t("speaker")}
              placeholder="Maxime Musterfrau"
              {...form.getInputProps("speaker")}
            />
          </div>
          <div className="col-span-1 md:col-span-2">
            <Text fz="sm">{t("description")}</Text>
            {editor && <Editor editor={editor} />}
          </div>
          <div>
            <DateInput
              placeholder="January 1, 1970"
              label={t("date")}
              withAsterisk
              {...form.getInputProps("date")}
            />
          </div>
          <div className="flex gap-4 items-end">
            <TimeInput
              label={t("time")}
              withAsterisk
              className="grow"
              {...form.getInputProps("startTime")}
            />
            <span className="mb-1">–</span>
            <TimeInput
              withAsterisk
              className="grow"
              {...form.getInputProps("endTime")}
            />
          </div>
          <div>
            <TextInput
              withAsterisk
              label={t("place")}
              placeholder="ETH HG F 7"
              {...form.getInputProps("place")}
            />
          </div>
          <div>
            <TextInput
              label={t("signUp")}
              placeholder="https://..."
              {...form.getInputProps("signUp")}
            />
          </div>
          <div className="col-span-1 md:col-span-2">
            <Checkbox
              label={t("stammtisch")}
              {...form.getInputProps("isStammtisch", { type: "checkbox" })}
            />
          </div>
          <div>
            <Button type="submit">{t("submit")}</Button>
          </div>
        </div>
      </form>
    </Modal>
  );
}
